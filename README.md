# Aide et ressources de Scripts DISCO pour Synchrotron SOLEIL

## Résumé

- Utilisation de python, sk.image, opencv, dask, pandas, jupyter, sortie en HDF h5py, stardist. Utilisés pour la segmentation, viewer napari/Qt pour la segmentation, I/O, recalage, interface iPyWidgets,
- Créé à Synchrotron Soleil

## Navigation rapide

| Wiki SOLEIL |
| - |
| [Tutoriaux](https://gitlab.com/soleil-data-treatment/soleil-disco/wikis/home) |

## Sources

- Code source: Oui, sur un dépôt ouvert en ligne (type Git, SourceForge, ...)
- Documentation officielle: Non

## Installation

- Systèmes d'exploitation supportés:
- Installation: Difficile (très technique)

## Format de données

- en entrée: TIFF, JSON,
- en sortie: HDF5
- Copie locale, puis sur la Ruche a la fin